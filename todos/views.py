from django.shortcuts import render
from todos.models import TodoItem, TodoList
from django.contrib.auth.decorators import login_required



@login_required
def list_todos(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)

